# -*- coding: utf-8 -*-

import memcache, random, time, timeit
from math import *

mc = memcache.Client(['194.29.175.241'], debug=1)

found = 0
total = 0

#liczby = []

def rozklad(x):
    # global liczby
    n = x
    # value = mc.get('p5_1:%d' % x)
    # print 'cache(%d): %s' % (x, str(value))
    # if value is None:
    if x<=0:
        return 0

    i=2
    e=floor(sqrt(x))
    r=[] #używana jest tablica (lista), nie bepośrednie wypisywanie
    while i<=e:
        value = mc.get('p5_11111:%d' % x)
        print 'cache(%d): %s' % (x, str(value))
        if value:
            lista = r + value
            mc.set('p5_11111:%d' % n, lista)
            return lista
        if x%i==0:
            r.append(i)
            #liczby.append(i)
            x/=i
            e=floor(sqrt(x))
        else:
            i+=1
    if x>1:
        value = mc.get('p5_11111:%d' % x)
        print 'cache(%d): %s' % (x, str(value))
        if value:
            return value

        r.append(x)
    #value = r

    value = mc.set('p5_11111:%d' % n, r)
    return r


def make_request():
    rozklad(random.randint(2, 5000))

l=1
while l>0:
    print("Podaj liczbę: ")
    l=int(input())
    r=rozklad(l)
    print r

# print 'Ten successive runs:',
# for i in range(1, 11):
#     print '%.2fs, ratio=%.2f' % (timeit.timeit(make_request, number=20), float(found) / total)
#     print